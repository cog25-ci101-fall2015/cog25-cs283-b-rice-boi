#include "csapp.c"
#include "client.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// decryptor_ - applies the modulo function to the currently passed in encrypted char value
//				returns the repsective character
// msg 		  - the current encrypted value
// keyExpon	  - the private key exponent value
// keyMod     - the value shared between the private and public key
int decryptor_(int msg, int keyExpon, int keyMod)
{
	int curI; // current char value
	curI = modulo(msg, keyExpon, keyMod);
	return curI;
}