#ifndef __CLIENT_H__
#define __CLIENT_H__

#include "csapp.c"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int getNthPrime (int N);
int mod_inverse(int a, int b);
int gcd(int a, int b);
int coprime(int n);
int modulo(int a, int b, int c);
int totient(int n);
int genKeyPt2(int phiNum);
int main(int argc, char **argv);

#endif /* __CLIENT_H__ */